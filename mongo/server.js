const MongoClient = require("mongodb").MongoClient;
   
const Joi = require('joi')
const express = require('express')
const app = express()
const path = require('path')

app.use(express.static(__dirname + '/dist/postApp')); 

app.use(express.json())

let db;

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname))
    // let q = req.query.loadQuantity
    // db.collection('posts').find().toArray((err, docs)=>{
    //     if(err) {
    //         console.log(err)
    //         return res.sendStatus(500)
    //     }
    //     res.send(docs.slice(0, q));
    // })
})

app.get('/details/:id', (req, res) => {
    let post;
    db.collection('posts').find().toArray((err, docs)=>{
        if(err) {
            console.log(err)
            return res.sendStatus(500)
        }
        //console.log(typeof docs)
        post = docs.find(e => {
            return ""+e['_id'] === req.params.id
        })
        if(!post) return res.status(404).send('Course wasn\'t found')
        else res.send(post);        
    })
})

app.post('/create', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    }

    const result = Joi.validate(req.body, schema)

    if(result.error) {
        return res.status(400).send(result.error.details[0].message)
    }
    
    const post = JSON.parse(req.get(formValue))
    db.collection('posts').insert(post, function(err, result){
        if(err) {
            return res.sendStatus(500)
        }
    })
    res.send(post)
})

const port = process.env.port || 4200

const url = "mongodb://localhost:27017/";

const mongoClient = new MongoClient(url, { useNewUrlParser: true });
 
// mongoClient.connect(function(err, client){
      
//     db = client.db("postsdb");
//     if(err){ 
//         return console.log(err);
//     }
//     const p = [{author: 'Anonim', date: '2019-02-21', description: 'dfgdks fjlkds jl fjs', img: 'dfg', keyCode: 289831, post: true, title: 'fgd', type: 'News'}]
//     app.listen(port, () => {
//         db.collection("posts").drop()
//         for (let pos of p) {
//             db.collection("posts").insertOne(pos, (err)=>{
//                 if (err) {
//                     return console.log(err)
//                 }
//             });
//         }
//         console.log(`Listening onds port ${port}`);
//     })
// });
const http = require('http')

const s = http.createServer(app);
s.listen(port, ()=> {console.log('Listeing')})