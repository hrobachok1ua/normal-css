import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { CreatePostPageComponent } from './create-post-page/create-post-page.component';
import { PostComponent } from './home-page/post/post.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { PostsAndUsersService } from './posts-and-users.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserPageComponent } from './user-page/user-page.component';
import { PostPageComponent } from './home-page/post-page/post-page.component';
import { NgxPaginationModule } from 'ngx-pagination';

import { ExitGuard } from './exit.guard';
import { DeleteMePlzComponent } from './delete-me-plz/delete-me-plz.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    RegisterPageComponent,
    CreatePostPageComponent,
    PostComponent,
    UserPageComponent,
    PostPageComponent,
    DeleteMePlzComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyDzw8sASA8nm1_bNh8Wt5CfVz93AoCbHCU',
      authDomain: 'postapp-f507f.firebaseapp.com',
      databaseURL: 'https://postapp-f507f.firebaseio.com',
      projectId: 'postapp-f507f',
      storageBucket: 'postapp-f507f.appspot.com',
      messagingSenderId: '75333077798'
    }),
    BrowserAnimationsModule,
    NgxPaginationModule,
    HttpClientModule
  ],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
  providers: [PostsAndUsersService, ExitGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
