import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('postsStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '50ms',
              animate(
                '550ms ease-out',
                style({ opacity: 1, transform: 'translateY(0px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit, DoCheck, OnDestroy {

  posts = [];
  // counter = 5;
  deletedArr = [];
  showBtn = true;

  currentDate = '2019-01-21'

  constructor(private postsAndUsersService: PostsAndUsersService, private router: Router, private activatedRoute: ActivatedRoute) {  }

  ngOnDestroy() {
    this.router.navigate(
      [], 
      {
        relativeTo: this.activatedRoute,
        queryParams: { loadQuantity: 5},
        queryParamsHandling: "merge"
      });
  }

  restrictShowDeleted(posts) {
    const d = this.postsAndUsersService.deletedArr;
    const p = posts;
    for (let dEl in d) {
      for (let el in p) {
        if (p[el].key === d[dEl].key) {
          p.splice(+el, 1);
        }
      }
    }
    return p;
  }

  // hideDeletedBin() {
  //   this.postsAndUsersService.showDeletedBin = false;
  // }

  showDeletedBin() {
    this.postsAndUsersService.showDeletedBin = !this.postsAndUsersService.showDeletedBin;
  }

  showMore() {
    let q;
    this.activatedRoute.queryParams.subscribe(p => {
      q = +p.loadQuantity
    })
    q+=5;
    this.router.navigate(
      [], 
      {
        relativeTo: this.activatedRoute,
        queryParams: { loadQuantity: q},
        queryParamsHandling: "merge"
      });
    this.loadPosts();
  }

  sortAsc(a, b) {
    if (a.date > b.date) {
      return 1;
    }
    if (a.date < b.date) {
      return -1;
    }
    return 0;
  }

  loadPosts() {
    if (this.postsAndUsersService.admin) {
      this.postsAndUsersService.getPosts()
      .subscribe(post => {
          this.posts = this.restrictShowDeleted(JSON.parse(""+post).sort(this.sortAsc).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
        });
      } else {
      this.postsAndUsersService.getPostsOnlySend()
      .subscribe(post => {console.log(JSON.parse(""+post))
        this.posts = this.restrictShowDeleted(JSON.parse(""+post).sort(this.sortAsc).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
     });
    }
  }

  ngDoCheck() {
    if (this.postsAndUsersService.shouldRender) {
      this.postsAndUsersService.shouldRender = false;
      this.loadPosts();
    }

    this.deletedArr = this.postsAndUsersService.deletedArr;
  }

  ngOnInit() {
    this.loadPosts();
  }

  onMethod(el) {
    if (el.checked) {
      this.loadPosts();
    } else if (!el.checked) {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          this.posts = this.restrictShowDeleted(JSON.parse(""+post).sort((a, b) => {
            if (a.date < b.date) {
              return 1;
            }
            if (a.date > b.date) {
              return -1;
            }
            return 0;
          }).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          this.posts = this.restrictShowDeleted(JSON.parse(""+post).sort((a, b) => {
            if (a.date < b.date) {
              return 1;
            }
            if (a.date > b.date) {
              return -1;
            }
            return 0;
          }).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
       });
      }
    }
  }

  onType(el) {
    if (this.postsAndUsersService.admin) {
      this.postsAndUsersService.getPosts()
      .subscribe(post => {
        const p = JSON.parse(""+post).sort(this.sortAsc);
        this.posts = this.restrictShowDeleted(p.filter(e => e.type === el.value).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
      });
    } else {
      this.postsAndUsersService.getPostsOnlySend()
      .subscribe(post => {
        const p = JSON.parse(""+post).sort(this.sortAsc);
        this.posts = this.restrictShowDeleted(p.filter(e => e.type === el.value).filter(el => el.date > this.currentDate));//.slice(0, this.counter)
     });
    }
  }

  onAuthor(el) {
    if (el.value === '') {
      this.loadPosts();
    } else {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          const p = JSON.parse(""+post).sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => e.author.includes(el.value))
          .filter(el => el.date > this.currentDate));//.slice(0, this.counter)
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          const p = JSON.parse(""+post).sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => e.author.toLowerCase().includes(el.value))
          .filter(el => el.date > this.currentDate));//.slice(0, this.counter)
       });
      }
    }
  }

  onSearch(el) {
    if (el.value === '') {
      this.loadPosts();
    } else {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          const p = JSON.parse(""+post).sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => (e.title.includes(el.value) || e.description.includes(el.value)))
          ).filter(el => el.date > this.currentDate);//.slice(0, this.counter)
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          const p = JSON.parse(""+post).sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => (e.title.includes(el.value) || e.description.includes(el.value)))
          ).filter(el => el.date > this.currentDate);//.slice(0, this.counter)
       });
      }
    }
  }

}
