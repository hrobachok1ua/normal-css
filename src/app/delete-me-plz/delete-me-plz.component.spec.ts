import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMePlzComponent } from './delete-me-plz.component';

describe('DeleteMePlzComponent', () => {
  let component: DeleteMePlzComponent;
  let fixture: ComponentFixture<DeleteMePlzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteMePlzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMePlzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
