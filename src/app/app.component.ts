import { Component, DoCheck, OnInit } from '@angular/core';
import { PostsAndUsersService } from './posts-and-users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements DoCheck, OnInit {

  text = 'Sign up/in';
  logged = false;
  showModal = false;

  constructor(public service: PostsAndUsersService, private r: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(p => {
      if (!p['loadQuantity']) {
        this.r.navigate(
          [], 
          {
            relativeTo: this.activatedRoute,
            queryParams: { loadQuantity: 5},
            queryParamsHandling: "merge"
          });
      }
    })
  }

  ngDoCheck() {
    if (this.service.userEmail !== '') {
      this.logged = true;
    } else {
      this.logged = false;
    }
  }

  hideChanged() {
    this.service.changed = false;
  }

  hideCheckEmail() {
    this.service.checkEmail = false;
    this.service.deletedAcc = false;
  }

  hideModal() {
    this.showModal = false;
  }

  hideAlreadyReg() {
    this.service.alreadyReg = false;
  }

  logOut() {
    this.showModal = true;
  }
  logOut2() {
    this.showModal = false;
    this.service.userEmail = '';
    this.service.admin = false;
    this.r.navigate(['/']);
  }
}
